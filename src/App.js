import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import '@vkontakte/vkui/dist/vkui.css';

import Home from './panels/Home';
import SelectType from './panels/SelectType';
import AddForm from './panels/AddForm';
import AddFormExt from './panels/AddFormExt';
import ResultSnippet from './panels/ResultSnippet';

const App = () => {
	const [activePanel, setActivePanel] = useState('home');

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
	}, []);

	const go = e => {
        setActivePanel(e.currentTarget.dataset.to);
    };

	return (
		<View activePanel={activePanel}>
			<Home id='home' go={go} />
			<SelectType id='selecttype' go={go} />
			<AddForm id='addfrom' go={go} />
			<AddFormExt id='addformext' go={go} />
			<ResultSnippet id='resultsnippet' go={go} />
		</View>
	);
}

export default App;

