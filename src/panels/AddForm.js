import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS } from '@vkontakte/vkui';
import View from '@vkontakte/vkui/dist/components/View/View';
import Select from '@vkontakte/vkui/dist/components/Select/Select';
import Textarea from '@vkontakte/vkui/dist/components/Textarea/Textarea';
import File from '@vkontakte/vkui/dist/components/File/File';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import FormLayout from '@vkontakte/vkui/dist/components/FormLayout/FormLayout';
import Input from '@vkontakte/vkui/dist/components/Input/Input';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Icon56GalleryOutline from '@vkontakte/icons/dist/56/gallery_outline';

const osName = platform();

const AddForm = ({ id, go }) => (
	<Panel id={id}>
		<View activePanel="new-user">
			<Panel id="new-user">
				<PanelHeader
					left={<PanelHeaderButton onClick={go} data-to="selecttype">
                        {osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
					</PanelHeaderButton>}
				>
					Целевой сбор
				</PanelHeader>
				<FormLayout>
					<File before={<Icon56GalleryOutline width={24} height={24}/>} controlSize="l">
						Загрузить обложку
					</File>
					<Input top="Название сбора" placeholder="Название сбора"/>
					<Input top="Сумма, ₽" placeholder="Сколько нужно собрать?"/>
					<Input top="Цель" placeholder="Например, лечение человека"/>

					<Textarea top="Описание" placeholder="На что пойдут деньги и как они кому-то помогут?" />

					<Select top="Куда получать деньги">
						<option value="vkpay">Счёт VK Pay · 1234</option>
						<option value="new_card">Новая карта</option>
					</Select>

					<Button size="xl"onClick={go} data-to="addformext">Далее</Button>
				</FormLayout>
			</Panel>
		</View>
	</Panel>
);

AddForm.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default AddForm;

