import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Icon28TargetOutline from '@vkontakte/icons/dist/28/target_outline';
import Icon28CalendarOutline from '@vkontakte/icons/dist/28/calendar_outline';

import Banner from '@vkontakte/vkui/dist/components/Banner/Banner';

const osName = platform();

const SelectType = ({ id, go }) => (
	<Panel id={id}>
		<PanelHeader
			left={<PanelHeaderButton onClick={go} data-to="home">
				{osName === IOS ? <Icon28ChevronBack width={24} height={24}/> : <Icon24Back/>}
			</PanelHeaderButton>}
		>
			Тип сбора
		</PanelHeader>
		<Banner
			before={<Icon28TargetOutline  width={24} height={24} fill={"#3F8AE0"}/>}
			header="Целевой сбор"
			subheader="Когда есть определённая цель"
			asideMode="expand"
			onClick={go}
			data-to="addfrom"
		/>
		<Banner
			before={<Icon28CalendarOutline width={24} height={24} fill={"#3F8AE0"}/>}
			header="Регулярный сбор"
			subheader="Если помощь нужна ежемесячно"
			asideMode="expand"
			onClick={go}
			data-to="addfrom"
		/>
	</Panel>
);

SelectType.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default SelectType;
