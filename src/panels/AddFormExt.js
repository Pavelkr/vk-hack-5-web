import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS } from '@vkontakte/vkui';
import View from '@vkontakte/vkui/dist/components/View/View';
import Select from '@vkontakte/vkui/dist/components/Select/Select';
import Radio from '@vkontakte/vkui/dist/components/Radio/Radio';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import FormLayout from '@vkontakte/vkui/dist/components/FormLayout/FormLayout';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';

const osName = platform();

const AddFormExt = ({ id, go }) => (
	<Panel id={id}>
		<View activePanel="new-user">
			<Panel id="new-user">
				<PanelHeader
					left={<PanelHeaderButton onClick={go} data-to="addfrom">
                        {osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
					</PanelHeaderButton>}
				>
					Дополнительно
				</PanelHeader>
				<FormLayout>
					<Select top="Сбор завершится">
						<option value="vkpay">Павел Кривошеев</option>
						<option value="new_card">Сообщество "Помощь зверям"</option>
					</Select>
					<Radio name="radio" value="1" defaultChecked>Когда соберём сумму</Radio>
					<Radio name="radio" value="2">В определённую дату</Radio>

					<Select top="Дата окончания" id={"select_date"}>
						<option value="20.09.2020">20 сентября</option>
						<option value="21.09.2020">21 сентября</option>
						<option value="22.09.2020">22 сентября</option>
						<option value="23.09.2020">23 сентября</option>
						<option value="24.09.2020">24 сентября</option>
					</Select>

					<Button size="xl" onClick={go} data-to="resultsnippet">Создать сбор</Button>
				</FormLayout>
			</Panel>
		</View>
	</Panel>
);

AddFormExt.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default AddFormExt;

